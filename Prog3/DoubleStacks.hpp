//
//  DoubleStacks.hpp
//  Prog3
//
//  Created by Rob MacDonald on 10/18/16.
//  Copyright © 2016 Rob MacDonald. All rights reserved.
//

#ifndef DoubleStacks_hpp
#define DoubleStacks_hpp

#include <stdio.h>
class DoubleStack
{
public:
    DoubleStack();         // Default constructor, the stack is empty to start
    ~DoubleStack();        // Default destructor
    
    void PushA(char value); // Add "value" to the top of stack A
    void PushB(char value); // Add "value" to the top of stack B
    
    char PopA(); // Remove and return the item on the top of stack A
    char PopB(); // Remove and return the item on the top of stack B
    
    char TopA(); // Return the item on the top of stack A
    char TopB(); // Return the item on the top of stack B
    
    unsigned int size();        // Return the number of items in the stack
    
private:
    
    unsigned int SizeA, SizeB, Total;
    
    char bigstack[20];
    
};

#endif /* DoubleStacks_hpp */
