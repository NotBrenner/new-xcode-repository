//
//  DoubleStacks.cpp
//  Program3Jermey
//
//  Created by Rob MacDonald on 10/17/16.
//  Copyright © 2016 Rob MacDonald. All rights reserved.
//

#include "DoubleStacks.hpp"
#include <iostream>

// Default constructor, the stack is empty to start
DoubleStack::DoubleStack()
{
    Total = 20;
    
    SizeA = 0;
    
    SizeB = 0;
    
    for (int i = 0; i > 20; i++)
        
    {
        bigstack[i] = '!';
    }
    
}


// Default destructor
DoubleStack::~DoubleStack()
{
    
}

// Add "value" to the top of stack A
void DoubleStack::PushA(char value)
{
    
    if(SizeA > 20)
        
        throw 5;
    
    
    if(SizeA + SizeB >= 20)
        
        throw 5;
    
    
    if( SizeA == 0)
        
    {
        SizeA++;
        
        bigstack[0] = value;
        
        return;
    }
    
    else
    
    {
        SizeA++;
        
        bigstack[SizeA - 1] = value;
        
        return;
        
    }
    
}

// Add "value" to the top of stack B
void DoubleStack::PushB(char value)
{
    
    if(SizeB > Total)
        
        throw 5;
    
    if(SizeA + SizeB >= 20)
        
        throw 5;
    
    if( SizeB == 0)
        
    {
        
        SizeB++;
        
        bigstack[19] = value;
        
        return;
        
    }
    
    else
        
    {
        
        SizeB++;
        
        bigstack[20-SizeB]=value;
        
        return;
    }
    
}

// Remove and return the item on the top of stack A
char DoubleStack::PopA()
{
    char lace;
    
    if(SizeA == 0)
        
        throw 5;
    
    if( SizeA == 1)
        
    {
        lace = bigstack[SizeA - 1];
        
        bigstack[0]= '!';
        
        SizeA--;
        
        return lace;
        
    }
    
    else
    
    {
        
        lace = bigstack[SizeA-1];
        
        bigstack[SizeA-1] = '!';
        
        SizeA--;
        
        return lace;
        
    }
    
    
}

// Remove and return the item on the top of stack B
char DoubleStack::PopB()
{
    char salute;
    
    if(SizeB == 0)
        
        throw 5;
    
    if(SizeB == 1)
        
    {
        salute =bigstack[19];
        
        bigstack[19]= '!';
        
        SizeB--;
        
        return salute;
    
    }
    
    else
    
    {
        salute  = bigstack[19 - 1];
        
        bigstack[19] = '!';
        
        SizeB--;
        
        return salute;
        
    }
    
}

// Return the item on the top of stack A
char DoubleStack::TopA()

{
    char ldrship;
    
    if(SizeA == 0)
        
        throw 5;
    
    ldrship = bigstack[SizeA-1];
    
    return ldrship;
    
}

// Return the item on the top of stack B
char DoubleStack::TopB()

{
    char opordr;
    
    if(SizeB == 0)
        
        throw 5;
    
    opordr = bigstack[20-SizeB];
    
    return opordr;
    
}

// Return the number of items in the stack
unsigned int DoubleStack::size()

{
    if(SizeA && SizeB == 0)
        
        return 0;
    
    else if (SizeA && SizeB == 20)
        
        return Total;
    
    else
        
        return SizeA + SizeB;
    
}